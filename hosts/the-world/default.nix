# #
##
##  per-host configuration for world
##
##

{ system, pkgs, dbPath, ... }: {
  inherit dbPath pkgs system;
  zfs-root = {
    boot = {
      devNodes = "/dev/disk/by-id/";
      bootDevices = [  "ata-SAMSUNG_MZNLN512HMJP-000L7_S2XANX0H510519" ];
      immutable = false;
      availableKernelModules = [
        # for booting virtual machine
        # with virtio disk controller
        "virtio_pci"
        "virtio_blk"
        # for sata drive
        "ahci"
        # for nvme drive
        "nvme"
        # for external usb drive
        "uas"
      ];
      removableEfi = false;
      kernelParams = [ ];
        sshUnlock = {
        # read sshUnlock.txt file.
        enable = false;
        authorizedKeys = [ ];
      };
    };
    networking = {
      # read changeHostName.txt file.
      hostName = "the-world";
      timeZone = "Europe/Zurich";
      hostId = "abcd1111";
    };
  };
}
