{ lib, ... }:
{
    dconf.enable = true;
    dconf.settings = with lib.hm.gvariant; {
      # gnome desktop
    
      "org/gnome/desktop/background" = {
        color-shading-type = "solid";
        picture-options = "zoom";
        picture-uri = ""; # if you want to use a file: "file:///home/manuel/Pictures/Wallpapers/hello-world.svg";
        picture-uri-dark = ""; # if you want to use a file: "file:///home/manuel/Pictures/Wallpapers/hello-world.svg";
        primary-color = "#6e6e6e";
      };
      "org/gnome/desktop/media-handling" = {        
        automount = false;
        automount-open = false;
      };
      "org/gnome/desktop/peripherals/mouse" = {
        natural-scroll = true;
      };
      "org/gnome/desktop/peripherals/touchpad" = {
        natural-scroll = true;
        tap-to-click = false;
      };
      "org/gnome/shell/window-switcher" = {
        current-workspace-only = false;
      };
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        # some apps don't support "color-scheme", therefore the following config is still necessary
        # (source: https://www.reddit.com/r/gnome/comments/tzs1r4/comment/i40yjzh/?utm_source=share&utm_medium=web3x):
        gtk-theme = "Adwaita-dark";
        show-battery-percentage = true;
      };
      
      "org/gnome/settings/daemon/plugins/power" = {
        power-button-action = "interactive"; # power button powers off machine (other options: 'suspend', 'nothing')
        power-saver-profile-on-low-battery = false;
        sleep-inactive-ac-timeout = 1200;
        sleep-inactive-ac-type = "nothing"; # nothing means "don't suspend"
        sleep-inactive-battery-timeout = 1200;
        sleep-inactive-battery-type = "nothing"; # nothing means "don't suspend"
        
      };

      # gnome keybindings
      # for keybinding examples visit https://the-empire.systems/nixos-gnome-settings-and-keyboard-shortcuts
      
      "org/gnome/desktop/wm/keybindings" = {
        panel-run-dialog = ["<Super>r"]; # instead of Alt+F2
        switch-applications = ["<Alt>Tab"];
        switch-applications-backward = ["<Shift><Alt>Tab"];
        switch-windows = ["<Super>Tab"];
        switch-windows-backward = ["<Shift><Super>Tab"];
      };

      # GNOME dash

      "org/gnome/shell" = {
        favorite-apps = ["org.gnome.Nautilus.desktop" "gnome-system-monitor.desktop" "com.raggesilver.BlackBox.desktop" "fedora-distrobox.desktop" "codium.desktop" "sqlitebrowser.desktop" "insomnia.desktop" "thunderbird.desktop" "firefox.desktop" "signal.desktop" "org.gnome.Lollypop.desktop" "anki.desktop" "standard-notes.desktop" "org.gnome.TextEditor.desktop" "bitwarden.desktop"];
      };
      
      # gnome text editor
      "org/gnome/TextEditor" = {
        discover-settings = false;
        restore-session = false;
        style-scheme = "builder-dark";
      };

      # black box
      "com/raggesilver/BlackBox" = { 
        fill-tabs = false;
        font = "FiraCode Nerd Font Mono 12";
        headerbar-drag-area = true;
        headerbar-draw-line-single-tab = false;
        scrollback-mode = 1; # history is unlimited
        scrollback-lines = 1000000; # fallback if (scrollback-mode=1 doesn't work)
        show-headerbar = true;
        style-preference = 2;
        theme-dark = "Monokai Dark"; # on helix set matching theme: "monokai_pro_spectrum"
      };

      # black box integration
      "com/github/stunkymonkey/nautilus-open-any-terminal" = {
        terminal = "blackbox";
        keybindings = "<Alt>t";
        new-tab = true;
      };

      # extensions:
      "org/gnome/shell/extensions/sane-airplane-mode" = {
        enable-airplane-mode = false;
        enable-bluetooth = false;
      };
      
      # "com/github/Ory0n/Resource_Monitor" = {
      #   cpuwidth = 35;
      #   diskstatsstatus = false;
      #   diskspacestatus = false;
      #   extensionposition = "center";
      #   netethstatus = false;
      #   netwlanstatus = false;
      #   ramunit = "perc";
      #   ramwidth = 35;
      #   refreshtime = 1;
      #   swapstatus = true;
      #   swapunit = "perc";
      #   swapwidth = 35;
      #   decimalsstatus = false;
      # };
      
      "org/gnome/shell/extensions/bluetooth-quick-connect" = {        
        refresh-button-on = true;
        show-battery-value-on = true;
      };
      "org/gnome/shell/extensions/just-perfection" = { 
        accessibility-menu = false;
        activities-button = false;
        animation = 3;
        app-menu = true;
        # "clock-menu-position" = 2;
        window-demands-attention-focus = true;
        workspace-switcher-should-show = true;
      };     
    };
    home.stateVersion = "22.11";
}