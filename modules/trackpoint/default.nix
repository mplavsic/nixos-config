# This disables the trackpoint. It works both when
# switching configuration and on restart. The timer
# is necessary for it to work. For more info visit:
# https://wiki.archlinux.org/title/TrackPoint#systemd.path_unit

{ config, pkgs, ... }:
{ 

# None of the following settings worked:

#    hardware.trackpoint.disable = false;
#
#    hardware.trackpoint = {
#      enable=true;
#      sensitivity=0;
#    };

# trackpoint-systemd-tmpfiles.nix also did not disable the trackpoint permanently

# I had to create these systemd timer, path and service units:

  systemd.timers.trackpoint_parameters = {
  wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "20s";
      Unit = "trackpoint_parameters.service";
    };
  };

  systemd.paths.trackpoint_parameters = {
    wantedBy = [ "default.target" ];
  	pathConfig = {
	    PathExists = /sys/devices/platform/i8042/serio1/serio2/sensitivity;
	    StopWhenUnneeded=true;
	  };
	  unitConfig = {
	    Description="Watch for, and modify, TrackPoint attributes";
	  };
  };

  systemd.services.trackpoint_parameters = {
    script = "echo -n 0 > /sys/devices/platform/i8042/serio1/serio2/sensitivity";
    # the speed attribute is no longer relevant after sensitivity is set to 0
 	  serviceConfig = {
      Description = "Set TrackPoint sensitivity attribute to 0 (i.e. disable TrackPoint)";
      Type = "oneshot";
      RemainAfterExit=true;
    };
  };
}