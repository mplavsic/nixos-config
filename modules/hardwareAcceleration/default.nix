{ config, pkgs, ... }:
{
  # old:
  #nixpkgs.config.packageOverrides = pkgs: {
  #  vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  #};
  hardware.opengl = {
    enable = true;
    # it will only work with packages from stable (and not unstable)
    extraPackages = builtins.attrValues {
      inherit (pkgs)
        intel-media-driver # LIBVA_DRIVER_NAME=iHD
        # old: vaapiIntel       # LIBVA_DRIVER_NAME=i965 (older but used to work better with Firefox/Chromium)
      ;
    };
  };
}
