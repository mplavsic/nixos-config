{ config, pkgs, ... }:

{
  # make the tailscale command usable to users
  environment.systemPackages = [ pkgs.tailscale ];

  # enable the tailscale service
  services.tailscale.enable = true;

  services.tailscale.port = 41641;

  networking.firewall.allowedUDPPorts = [ 41641 ];
  # I previously tried to set the ports to [ ${services.tailscale.port} ];,
  # however that syntax did not work. 
  # Would be nice to replace with the right syntax at some point.

  networking.firewall.checkReversePath = "loose";
}