{
    inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # programsdb is used to enable command-not-found with flakes (maybe needed cause of fish)
    programsdb = {
      url = "github:wamserma/flake-programs-sqlite";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # xremap-flake.url = "github:xremap/nix-flake"; # todo: remove in the future
  };
  
  outputs = { self, nixpkgs, programsdb }@inputs:
    let
      pkgs-lib = nixpkgs.lib;
      mkHost = { zfs-root, pkgs, dbPath, system, ... }:
        pkgs-lib.nixosSystem {
          inherit system;
          modules = [
            ./modules
            #import xremap-flake.nixosModules.default
            (import ./configuration.nix { inherit zfs-root inputs pkgs pkgs-lib dbPath; })
            ({...}: {
              nix.settings.experimental-features = [ "nix-command" "flakes" ];
              # `nix search nixpkgs just-perfection` will invoke `nix search --experimental-features 'nix-command flakes' nixpkgs just-perfection`
              nix.registry.pkgs.flake = nixpkgs;
              # now you can use: nix search pkgs thunderbird
              
              # by not assigning to nixpkgs, default's nixpkgs stays github:NixOS/nixpkgs/nixpkgs-unstable (in registry)
            })
          ];
        };
    in {
      nixosConfigurations = {
        the-world = let
          pkgs = nixpkgs.legacyPackages.${system};
          system = "x86_64-linux";
          dbPath = programsdb.packages.${pkgs.system}.programs-sqlite;
        in mkHost (import ./hosts/the-world { inherit system pkgs dbPath ; });
        # exampleHost = let
        #   pkgs = nixpkgs.legacyPackages.${system};
        #   system = "x86_64-linux";
        # in mkHost (import ./hosts/exampleHost { inherit system pkgs; });
      };
    };
}
