{ zfs-root, inputs, pkgs, pkgs-lib, dbPath, ... }: {

  # load module config to here
  inherit zfs-root;
          
  # Let 'nixos-version --json' know about the Git revision
  # of this flake.
  system.configurationRevision = if (inputs.self ? rev) then
    inputs.self.rev
  else
    throw "refuse to build: git tree is dirty";

  nixpkgs.config.allowUnfree = true;

  # system.stateVersion is never supposed to be changed. It just marks the version you
  # installed NixOS on, so that upstream knows for backwards compatibility.
  system.stateVersion = "22.11";

  # Enable NetworkManager for wireless networking,
  # You can configure networking with "nmtui" command.
  networking.useDHCP = false;
  networking.networkmanager.enable = true;

  # Let's set Quad9 as DNS provider.
  networking.nameservers = [ "2620:fe::fe" "2620:fe::9" "9.9.9.9" "149.112.112.112"];
  
  # Let's configure systemd-resolved to use DNS over TLS, DNSSEC.
  services.resolved.enable = true;
  services.resolved.dnssec = "true";
  services.resolved.extraConfig = ''
    DNSOverTLS=yes
  '';

  # For printers
  services.printing.enable = true;
  services.avahi.enable = true;
  services.avahi.nssmdns = true;
  # for a WiFi printer
  services.avahi.openFirewall = true;

  # Scanners
  hardware.sane.enable = true;
  hardware.sane.brscan4.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_IE.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_IE.UTF-8";
    LC_IDENTIFICATION = "en_IE.UTF-8";
    LC_MEASUREMENT = "en_IE.UTF-8";
    LC_MONETARY = "en_IE.UTF-8";
    LC_NAME = "en_IE.UTF-8";
    LC_NUMERIC = "en_IE.UTF-8";
    LC_PAPER = "en_IE.UTF-8";
    LC_TELEPHONE = "en_IE.UTF-8";
    LC_TIME = "en_IE.UTF-8";
  };

  # Configure keymap in X11
  #services.xserver = {
  #  layout = "en";
  #  xkbVariant = "";
  #};

  # Configure console keymap
  # console.keyMap = "en";

  # services.xremap.withGnome = true;

  services.xserver.xkbOptions = "altwin:ctrl_alt_win,caps:swapescape,compose:ralt,terminate:ctrl_alt_bksp,lv3:lsgt_switch";
  console.useXkbConfig = true;

  # Enable sound with pipewire.
  sound.enable = false; # this conflics with pipewire
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  virtualisation = {
    podman = {
      enable = true;
      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;
      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled.enable = true;
      extraPackages = [ pkgs.zfs ];
    };
    oci-containers.backend = "podman";
    containers.storage.settings = {
      storage = {
        driver = "zfs";
        graphroot = "/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };
    };
  };

  # Enable GNOME
  # GNOME must be used with a normal user account.
  # However, by default, only root user is configured.
  # Create a normal user and set password.
  #
  # You need to enable all options in this attribute set.
  services.xserver = {
    enable = true;
    desktopManager.gnome.enable = true;
    displayManager.gdm.enable = true;
    excludePackages = [ pkgs.xterm ];
  };

  users.users = {
    root = {
      initialHashedPassword = "$6$LkTcmTbClARMm95h$6ezw2foTtxHbZe9FK1rKnX3Wpkc1m.S2ZpWwxSwx7MVdkBpccL3O73fQuEVyUOCL8g/2d6gXB68.n4yam7w0K/";
      openssh.authorizedKeys.keys = [ "sshKey_placeholder" ];
    };

    manuel = {
      isNormalUser = true;
      description = "Manuel";
      extraGroups = [ "adbusers" "networkmanager" "wheel" "scanner" "lp" ];
      packages = with pkgs; [
      ];
    };
  };

  environment.variables.SUDO_EDITOR = "hx";
  environment.variables.VISUAL = "hx";
  environment.variables.EDITOR = "hx";

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting "🐟"
    '';
  };

  programs.starship = {
    enable = true;
  };

  users.defaultUserShell = pkgs.fish;

  imports = [
    "${inputs.nixpkgs}/nixos/modules/installer/scan/not-detected.nix"
    # "${inputs.nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
  ];

  services.openssh = {
    enable = pkgs-lib.mkDefault true;
    settings = { PasswordAuthentication = pkgs-lib.mkDefault false; };
  };

  boot.supportedFilesystems = [ "ntfs" ];
  boot.zfs.forceImportRoot = pkgs-lib.mkDefault false;
  # boot.kernelParams = [ "zfs.zfs_arc_max=2147483648" ];
  
  programs.git = {
    enable = true;
  };
  
  programs.command-not-found.enable = true;
  programs.command-not-found.dbPath = dbPath;

  security = {
    doas.enable = pkgs-lib.mkDefault false;
    sudo = {
      enable = pkgs-lib.mkDefault true;
      extraRules = [
        { users = [ "manuel" ];
          commands = [
            { command = "ALL" ;
              options= [ "NOPASSWD" ]; # "SETENV" # Adding the following could be a good idea
            }
          ];
        }
      ];
    };
  };

  environment.sessionVariables.QT_QPA_PLATFORM="wayland";
  environment.sessionVariables.NIXOS_OZONE_WL="1";

 programs.dconf.enable = true; # important for home-manager

#  disabled at the moment because impure:
 systemd.tmpfiles.rules =
   let
     monitorsXmlContent = builtins.readFile /home/manuel/.config/monitors.xml;
     monitorsConfig = pkgs.writeText "gdm_monitors.xml" monitorsXmlContent;
   in
     [
       "L+ /run/gdm/.config/monitors.xml - - - - ${monitorsConfig}"
     ];

  environment.systemPackages = builtins.attrValues {
    inherit (pkgs)
      firefox
      gnome-browser-connector
      thunderbird
      jq
      ffmpeg
      anki-bin
      gimp-with-plugins
      shortwave
      nautilus-open-any-terminal # to add "open in blackbox" option in context menu
      # glib # dependency for nautilus-open-any-terminal; this one can be installed through nix-shell
      # NB: uncomment glib if the nix-shell isn't enough
      wl-clipboard
      insomnia
      bitwarden
      standardnotes
      marktext
      podman-compose
      helix
      sqlitebrowser
      blackbox-terminal
      distrobox
      signal-desktop
      tdesktop
      qbittorrent
      mpv
      remmina
      restic
      libheif # needed in combination with pkgs.gnome.eog for iPhone pics
      lollypop
      vscodium
      nodejs
    ;
    
    inherit (pkgs.xorg)
      xeyes
    ;

    inherit (pkgs.gnome)
      dconf-editor
      gnome-tweaks
      nautilus-python # to add "open in blackbox" option in context menu
      gnome-session
      eog
    ;
  };

  environment.gnome.excludePackages = builtins.attrValues {
    inherit (pkgs)
      gnome-photos
      gnome-tour
      gnome-console # use blackbox instead
    ;

    inherit (pkgs.gnome)
      cheese # webcam tool
      # gnome-disk-utility # Use nix shell for this
      gnome-music
      gnome-terminal # use blackbox instead
      gedit # text editor
      epiphany # web browser
      geary # email reader
      # evince # document viewer
      # gnome-characters
      totem # video player
      tali # poker game
      iagno # go game
      hitori # sudoku game
      atomix # puzzle game
    ;
  };

  fonts.packages = with pkgs;
    [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code # monospace font w/ ligatures
      fira-code-symbols
      fira-mono # monospace font
      (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
    ];

}
